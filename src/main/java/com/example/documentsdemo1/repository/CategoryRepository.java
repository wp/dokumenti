package com.example.documentsdemo1.repository;

import com.example.documentsdemo1.model.Category;
import com.example.documentsdemo1.service.CategoryService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category>findAllByNameLike(String name);
}
