package com.example.documentsdemo1.service;


import com.example.documentsdemo1.model.User;

public interface AuthService {
    User login(String username, String password);
}
