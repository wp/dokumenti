package com.example.documentsdemo1.service;

import com.example.documentsdemo1.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    Category add (String name, String description);
    List<Category> findAll();
    Optional<Category> findById(Long id);
    List<Category> listAllCategoriesByName(String name);
    Category edit(Long id, String name, String description);
}
