package com.example.documentsdemo1.service;

import com.example.documentsdemo1.model.Employee;

public interface EmployeeService {
    Employee create(String employee_id, String name, String surname);
}
