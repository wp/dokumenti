package com.example.documentsdemo1.service.Impl;

import com.example.documentsdemo1.model.User;
import com.example.documentsdemo1.model.enumerations.UserRole;
import com.example.documentsdemo1.model.exceptions.InvalidUsernameOrPasswordException;
import com.example.documentsdemo1.repository.UserRepository;
import com.example.documentsdemo1.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(Long id, String username, String password, String role) {
        User user = new User(id, username, password, role);
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username, String password) {
        if(username == null || password == null || username.isEmpty() || password.isEmpty()) {
            throw new InvalidUsernameOrPasswordException();
        }
        return userRepository.findByUsernameAndPassword(username, password);
    }



    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s).orElseThrow(()->new UsernameNotFoundException(s));
    }



}
