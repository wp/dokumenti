package com.example.documentsdemo1.service.Impl;

import com.example.documentsdemo1.model.Category;
import com.example.documentsdemo1.model.Document;
import com.example.documentsdemo1.model.Employee;
import com.example.documentsdemo1.model.exceptions.CategoryNotFoundException;
import com.example.documentsdemo1.model.exceptions.EmployeeNotFoundException;
import com.example.documentsdemo1.repository.CategoryRepository;
import com.example.documentsdemo1.repository.DocumentRepository;
import com.example.documentsdemo1.repository.EmployeeRepository;
import com.example.documentsdemo1.service.DocumentService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements DocumentService {
    private final DocumentRepository documentRepository;
    private final CategoryRepository categoryRepository;
    private final EmployeeRepository employeeRepository;
    public DocumentServiceImpl(DocumentRepository documentRepository, CategoryRepository categoryRepository, EmployeeRepository employeeRepository) {
        this.documentRepository = documentRepository;
        this.categoryRepository = categoryRepository;
        this.employeeRepository = employeeRepository;
    }

    //TODO: Implement add the ID of the employee
    @Override
    public Document add(String name, MultipartFile file, String fileExtension, String description, boolean studentPermission, boolean professorPermission, boolean administrationPermission, Long categoryId) {
        try {
            Category category = this.categoryRepository.findById(categoryId)
                    .orElseThrow(() -> new CategoryNotFoundException(categoryId));
            Employee employee = this.employeeRepository.findById("prof.prof")
                    .orElseThrow(() -> new EmployeeNotFoundException("prof.prof"));

            byte[] fileData = file.getBytes();

            Document document = new Document(name, fileData, fileExtension,description, studentPermission, professorPermission, administrationPermission, category, employee);
            return this.documentRepository.save(document);
        } catch (IOException e) {

            e.printStackTrace();
        }

        return null;
    }


    @Override
    public List<Document> findAll() {
        return this.documentRepository.findAll();
    }

    @Override
    public Optional<Document> findById(Long documentId) {
        return this.documentRepository.findById(documentId);
    }

    @Override
    public void edit(Long id, String name, MultipartFile file, String description, boolean studentPermission, boolean professorPermission, boolean administrationPermission) {
        Document document = this.documentRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Document not found with id: " + id));

        String originalFileName = file.getOriginalFilename();
        String extension = "";

        if (originalFileName != null && !originalFileName.isEmpty()) {
            int dotIndex = originalFileName.lastIndexOf('.');
            if (dotIndex > 0 && dotIndex < originalFileName.length() - 1) {
                extension = originalFileName.substring(dotIndex + 1);
            }
        }

        if (!file.isEmpty()) {
            try {
                byte[] fileData = file.getBytes();
                document.setFilePath(fileData);
            } catch (IOException e) {
                throw new RuntimeException("Failed to read file data: " + e.getMessage(), e);
            }
        }

        document.setFileExtension(extension);
        document.setName(name);
        document.setDescription(description);
        document.setStudentPermission(studentPermission);
        document.setProfessorPermission(professorPermission);
        document.setAdministrationPermission(administrationPermission);
        this.documentRepository.save(document);
    }

    @Override
    public void delete(Long id) {
        this.documentRepository.deleteById(id);
    }


}
