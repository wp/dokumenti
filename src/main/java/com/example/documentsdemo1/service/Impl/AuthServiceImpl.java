package com.example.documentsdemo1.service.Impl;


import com.example.documentsdemo1.model.User;
import com.example.documentsdemo1.model.exceptions.InvalidArgumentsException;
import com.example.documentsdemo1.model.exceptions.InvalidUserCredentialsException;
import com.example.documentsdemo1.repository.UserRepository;
import com.example.documentsdemo1.service.AuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;

    public AuthServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User login(String username, String password) {
        if (username==null || username.isEmpty() || password==null || password.isEmpty()) {
            throw new InvalidArgumentsException();
        }
        return userRepository.findByUsernameAndPassword(username,
                password).orElseThrow(InvalidUserCredentialsException::new);
    }

}
