package com.example.documentsdemo1.service.Impl;

import com.example.documentsdemo1.model.Student;
import com.example.documentsdemo1.repository.StudentRepository;
import com.example.documentsdemo1.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student create(Long student_id, String name, String surname) {
        Student student = new Student(student_id, name, surname);
        return studentRepository.save(student);
    }
}
