package com.example.documentsdemo1.service.Impl;

import com.example.documentsdemo1.model.Employee;
import com.example.documentsdemo1.repository.EmployeeRepository;
import com.example.documentsdemo1.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee create(String employee_id, String name, String surname) {
        Employee employee = new Employee(name + "." + surname, name, surname);
        return employeeRepository.save(employee);
    }
}
