package com.example.documentsdemo1.service.Impl;

import com.example.documentsdemo1.model.Category;
import com.example.documentsdemo1.repository.CategoryRepository;
import com.example.documentsdemo1.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category add(String name, String description) {
        Category category = new Category(name, description);
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> findAll() {
        return this.categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return this.categoryRepository.findById(id);
    }

    @Override
    public List<Category> listAllCategoriesByName(String name) {
        String nameLike="%" + name + "%";
        if (name!=null){
            return this.categoryRepository.findAllByNameLike(nameLike);
        }else {
            return this.categoryRepository.findAll();
        }
    }
    @Override
    public Category edit(Long id, String name, String description) {
        Optional<Category> category=findById(id);
        category.get().setName(name);
        category.get().setDescription(description);
        return this.categoryRepository.save(category.get());
    }
}
