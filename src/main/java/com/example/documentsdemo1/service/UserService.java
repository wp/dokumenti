package com.example.documentsdemo1.service;

import com.example.documentsdemo1.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {
    User create(Long id, String username, String password, String role);
    Optional<User> findByUsername(String username, String password);
}
