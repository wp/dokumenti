package com.example.documentsdemo1.service;

import com.example.documentsdemo1.model.Document;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface DocumentService {
    public Document add(String name, MultipartFile file, String fileExtension, String description, boolean student, boolean professor, boolean administration, Long categoryId);
    List<Document> findAll();
    Optional<Document> findById(Long documentId);
    public void edit(Long id, String name, MultipartFile file, String description, boolean studentPermission, boolean professorPermission, boolean administrationPermission);
    void delete(Long id);
}
