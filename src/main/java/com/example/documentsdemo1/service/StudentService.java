package com.example.documentsdemo1.service;

import com.example.documentsdemo1.model.Student;

public interface StudentService {
    Student create(Long student_id, String name, String surname);
}
