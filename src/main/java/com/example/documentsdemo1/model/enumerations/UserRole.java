package com.example.documentsdemo1.model.enumerations;

import org.springframework.security.core.GrantedAuthority;

public enum UserRole implements GrantedAuthority {
    PROFESSOR,
    ADMINISTRATION,
    STUDENT;

    @Override
    public String getAuthority() {
        return name();
    }

}