package com.example.documentsdemo1.model;

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
public class Student {
    @Id
    @Column(name = "student_id")
    private Long id;
    private String name;
    private String surname;

    public Student() {

    }

    public Student(Long id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

}
