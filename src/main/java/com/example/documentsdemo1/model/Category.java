package com.example.documentsdemo1.model;

import javax.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long id;
    private String name;

    private String description;
    @OneToMany(mappedBy = "category")
    private List<Document> documents;

    public Category() {
    }

    public Category(String name, String description){
        this.name = name;
        this.description = description;
    }

    public Category(String name, List<Document> documents) {
        this.name = name;
        this.documents = documents;
    }
}
