package com.example.documentsdemo1.model;

import javax.persistence.*;
import lombok.Data;
import org.springframework.data.repository.cdi.Eager;

@Data
@Entity
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    private Long id;
    private String name;
    @Column(name = "file_path")
    @Lob
    private byte[] filePath;
    @Column(name = "file_extension")
    private String fileExtension;
    @Column(length = 4000)
    private String description;
    @Column(name = "student_permission")
    private boolean studentPermission;
    @Column(name = "professor_permission")
    private boolean professorPermission;
    @Column(name = "administartion_permission")
    private boolean administrationPermission;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne()
    @JoinColumn(name = "employee_id")
    private Employee employee;



    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String hasStudentPermission() {
        if (studentPermission) {
            return "Student";
        } else {
            return "";
        }
    }

    public String hasProfessorPermission(boolean professorPermission) {
        if (professorPermission) {
            return "Professor";
        } else return "";
    }
    public String hasAdministrationPermission(boolean administrationPermission) {
        if (administrationPermission) {
            return "Administration";
        } else return "";
    }

    public Document() {
    }

    public Document(String name, byte[] filePath, String fileExtension,String description,
                    boolean studentPermission, boolean professorPermission, boolean administrationPermission, Category category, Employee employee) {
        this.name = name;
        this.filePath = filePath;
        this.fileExtension = fileExtension;
        this.description = description;
        this.studentPermission = studentPermission;
        this.professorPermission = professorPermission;
        this.administrationPermission = administrationPermission;
        this.category = category;
        this.employee = employee;
    }
}
