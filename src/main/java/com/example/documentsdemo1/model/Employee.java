package com.example.documentsdemo1.model;

import com.example.documentsdemo1.model.enumerations.UserRole;
import javax.persistence.*;
import lombok.Data;

@Data
@Entity
public class Employee {
    @Id
    @Column(name = "employee_id")
    private String id;
    private String name;
    private String surname;
    @Enumerated(value = EnumType.STRING)
    private UserRole userRole;

    public Employee() {
    }

    public Employee(String id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public Employee(String id, String name, String surname, UserRole userRole) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.userRole = userRole;
    }
}
