package com.example.documentsdemo1.web.controller;

import com.example.documentsdemo1.model.Category;
import com.example.documentsdemo1.model.Document;
import com.example.documentsdemo1.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = {"/categories", "/"})
public class CategoriesController {
    private final CategoryService categoryService;

    public CategoriesController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping()
    public String showAllCategories(Model model, @RequestParam(required = false) String nameSearch) {
        List<Category> categories;
        if (nameSearch == null) {
            categories = this.categoryService.findAll();
        } else {
            categories = this.categoryService.listAllCategoriesByName(nameSearch);
        }
        model.addAttribute("categories", categories);

        return "categories";
    }

    @GetMapping("/{id}")
    public String showCategory(@PathVariable Long id, Model model) {
        Optional<Category> category = this.categoryService.findById(id);
        List<Document> documentsInCategory = category.get().getDocuments();
        Long categoryId = category.get().getId();
        model.addAttribute("category", category.get().getName());
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("documentsInCategory", documentsInCategory);
        return "category";
    }

    @GetMapping("/add-form")
    public String addCategoryPage(Model model) {
        List<Category> categories = this.categoryService.findAll();
        model.addAttribute("categories", categories);
        return "add-category";
    }

    @GetMapping("/{id}/edit")
    public String showEdit(@PathVariable Long id, Model model) {
        model.addAttribute("categories", this.categoryService.findById(id));
        return "edit-category";
    }

    @PostMapping("/add")
    public String addCategory(@RequestParam String name,
                              @RequestParam String description) {
        this.categoryService.add(name, description);
        return "redirect:/categories";
    }

    @PostMapping("/{id}/edit-form")
    public String editCategory(@PathVariable Long id,
                       @RequestParam String name,
                       @RequestParam String description) {
        this.categoryService.edit(id, name, description);
        return "redirect:/categories";
    }

}
