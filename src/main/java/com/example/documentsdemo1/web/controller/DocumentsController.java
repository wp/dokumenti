package com.example.documentsdemo1.web.controller;

import com.example.documentsdemo1.model.Category;
import com.example.documentsdemo1.model.Document;
import com.example.documentsdemo1.service.CategoryService;
import com.example.documentsdemo1.service.DocumentService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.FileNotFoundException;
import java.util.List;

@Controller
@RequestMapping("/documents")
public class DocumentsController {

    private final DocumentService documentService;
    private final CategoryService categoryService;
    public DocumentsController(DocumentService documentService, CategoryService categoryService) {
        this.documentService = documentService;
        this.categoryService = categoryService;
    }

    @GetMapping("/{categoryId}/add-form")
    public String addDocumentPage(@PathVariable Long categoryId, Model model) {
        List<Document> documents = this.documentService.findAll();
        String categoryName = this.categoryService.findById(categoryId).get().getName();
        model.addAttribute("documents", documents);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("categoryName", categoryName);
        return "add-document";
    }

    private String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
            return fileName.substring(dotIndex + 1);
        }
        return ""; // Return an empty string if no extension is found
    }

    @PostMapping("/add")
    public String addDocument(@RequestParam String name,
                              @RequestParam MultipartFile file,
                              @RequestParam String description,
                              @RequestParam(required = false) boolean studentPermission,
                              @RequestParam(required = false) boolean professorPermission,
                              @RequestParam(required = false) boolean administrationPermission,
                              @RequestParam Long categoryId,
                              RedirectAttributes redirectAttributes) {
        String fileExtension = getFileExtension(file.getOriginalFilename());
        this.documentService.add(name, file, fileExtension,description, studentPermission, professorPermission, administrationPermission, categoryId);
        redirectAttributes.addAttribute("id", categoryId);
        return "redirect:/categories/{id}";
    }

    @GetMapping("/category/{categoryId}/document/{documentId}/edit-form")
    public String editDocumentPage(@PathVariable Long categoryId,
                                   @PathVariable Long documentId,
                                   Model model) {
        Document document = this.documentService.findById(documentId).get();
        model.addAttribute("document", document);
        model.addAttribute("categoryId", categoryId);
        List<Category> categories = this.categoryService.findAll();
        model.addAttribute("categories", categories);
        return "edit-document";
    }

    @PostMapping("/{id}/edit")
    public String editDocument(@PathVariable Long id,
                               @RequestParam String name,
                               @RequestParam MultipartFile file,
                               @RequestParam String description,
                               @RequestParam(required = false) boolean studentPermission,
                               @RequestParam(required = false) boolean professorPermission,
                               @RequestParam(required = false) boolean administrationPermission,
                               @RequestParam Long categoryId,
                               RedirectAttributes redirectAttributes) {
        this.documentService.edit(id, name, file, description, studentPermission, professorPermission, administrationPermission);
        redirectAttributes.addAttribute("id", categoryId);
        return "redirect:/categories/{id}";
    }

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable Long fileId) throws FileNotFoundException {
        Document doc = documentService.findById(fileId).orElseThrow(() -> new FileNotFoundException("Document not found"));
        ByteArrayResource resource = new ByteArrayResource(doc.getFilePath());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + doc.getName() + "." + doc.getFileExtension() + "\"")

                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(doc.getFilePath().length)
                .body(resource);
    }


    @PostMapping("/{documentId}/delete")
    public String deleteDocument(@PathVariable Long documentId, RedirectAttributes redirectAttributes) {
        Document document = documentService.findById(documentId).get();
        this.documentService.delete(documentId);
        Category category = categoryService.findById(document.getCategory().getId()).get();
        long id = category.getId();
        redirectAttributes.addAttribute("id", id);
        return "redirect:/categories/{id}";
    }
}
