package com.example.documentsdemo1.web.controller;

import com.example.documentsdemo1.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/login")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String loginPage() {
        return "login";
    }

    @PostMapping("/loginForm")
    public String loginForm(@RequestParam String username, @RequestParam String password) {

        userService.findByUsername(username, password);
        return "redirect:/categories";
    }
}
