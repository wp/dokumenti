package com.example.documentsdemo1.config;

import com.example.documentsdemo1.model.*;
import com.example.documentsdemo1.model.enumerations.UserRole;
import com.example.documentsdemo1.repository.*;
import javax.annotation.PostConstruct;
import javax.management.relation.Role;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.util.List;

import static com.example.documentsdemo1.model.enumerations.UserRole.PROFESSOR;

@Component
public class DataInitializer {
    private final CategoryRepository categoryRepository;
    private final StudentRepository studentRepository;
    private final DocumentRepository documentRepository;
    private final EmployeeRepository employeeRepository;
    private final UserRepository userRepository;

    public DataInitializer(CategoryRepository categoryRepository, StudentRepository studentRepository, DocumentRepository documentRepository, EmployeeRepository employeeRepository, UserRepository userRepository) {
        this.categoryRepository = categoryRepository;
        this.studentRepository = studentRepository;
        this.documentRepository = documentRepository;
        this.employeeRepository = employeeRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void init() throws IOException {
        //CATEGORIES
        categoryRepository.save(new Category("Заверка на семестар", "Документи потребни " +
                "за заверка на летен семестар 2022/2023 година"));
        categoryRepository.save(new Category("Упис на семестар", "Документи потребни за " +
                "упис на зимски семестар 2023/2024 година"));
        categoryRepository.save(new Category("Дипломирање", "Документи потребни за " +
                "извршување и одбрана на дипломски труд"));
        categoryRepository.save(new Category("Еразмус+", "Документи за извршување на " +
                "студентска размена во 2023/2024 година"));
        List<Category> categories = categoryRepository.findAll();

        //STUDENTS
        Student student1 = new Student((long) (203213), "Bojan", "Dimitrovski");
        Student student2 = new Student((long) (203007), "Maja", "Vuevska");
        Student student3 = new Student((long) (203013), "Nadica", "Dimitrovska");
        this.studentRepository.save(student1);
        this.studentRepository.save(student2);
        this.studentRepository.save(student3);

        //EMPLOYEES
        Employee employee1 = new Employee("prof.prof", "Prof", "Prof", PROFESSOR);
        Employee employee2 = new Employee("sluzba.sluzba", "Sluzba", "Sluzba", UserRole.ADMINISTRATION);
        this.employeeRepository.save(employee1);
        this.employeeRepository.save(employee2);

        //USERS
        User user1 = new User( 1L, "203213", "pass123", UserRole.STUDENT.toString());
        User user2 = new User( 2L, "sasho_gramatikov", "prof", UserRole.PROFESSOR.toString());
        this.userRepository.save(user1);
        this.userRepository.save(user2);

        //DOCUMENTS
        ClassPathResource resource = new ClassPathResource("files/da.pdf");
        byte[] fileData = StreamUtils.copyToByteArray(resource.getInputStream());

        String fileName = resource.getFilename();  // Get the actual file name
        String extension = "";

        int dotIndex = fileName.lastIndexOf('.');
        if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
            extension = fileName.substring(dotIndex + 1);
        }

        //Document that the student can see
        Document document1 = new Document("Образец за заверка на семестар",
                fileData,
                extension,
                "опис",
                true,
                true,
                true,
                categories.get(0),
                employee2);
        Document document2 = new Document("Образец за запишување на семестар",
                fileData,
                extension,
                "опис",
                false,
                true,
                true,
                categories.get(1),
                employee2);
        Document document3 = new Document("Пријава за Дипломски труд",
                fileData,
                extension,
                "опис",
                false,
                true,
                true,
                categories.get(2),
                employee2);
        Document document4 = new Document("Пријавен лист - ВОИ 20",
                fileData,
                extension,
                "опис",
                false,
                true,
                true,
                categories.get(0),
                employee2);
        Document uplatnica = new Document("Уплатница за школарина за државна квота",
                fileData,
                extension,
                "опис",
                false,
                true,
                true,
                categories.get(1),
                employee2);
        this.documentRepository.save(document1);
        this.documentRepository.save(document2);
        this.documentRepository.save(document3);
        this.documentRepository.save(document4);
        this.documentRepository.save(uplatnica);
    }
}